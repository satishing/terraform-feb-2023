provider "time" {}

terraform {
  required_providers {
    time = {
      source  = "hashicorp/time"
      version = "0.9.1"
    }
  }
}

resource "time_static" "example" {}

output "current_time" {
  value = time_static.example.rfc3339
}

output "current_time_1" {
  value = time_static.example.id
}