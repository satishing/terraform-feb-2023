# Session - 008

## More providers
- [publicip](https://registry.terraform.io/providers/nxt-engineering/publicip/latest/docs) 
- [tls](https://registry.terraform.io/providers/hashicorp/tls/latest/docs)
- [time](https://registry.terraform.io/providers/hashicorp/time/latest/docs)

## Provisioners

- Provisioners are used to model specific actions on the local machine or on a remote machine in order to prepare servers or other infrastructure objects for service.
- Terraform cannot model the actions of provisioners as part of a plan because they can in principle take any action.
- Provisioner needs direct network access to your servers, issuing Terraform credentials to log in, making sure that all the necessary external software is installed, etc.
- Provisioners should be used as a last resort, you should use alternatives like `user_data` on aws instance or `custom_data` on azure vm.
- It is not recommended to configure system at the time of resource provisioning. Instead, use any configuration management tool.


## Types of provisioners
- file
- local-exec
- remote-exec

### file
Copies file or directory on remote resource.
```terraform
resource "azurerm_virtual_machine" "this" {
  #...
  
  provisioner "file" {
    source = "/tmp/config.xml"
    destination = "/etc/app/config.xml"
    
    connection {
      type = "ssh/winrm"
      host = azurerm_public_ip.this.ip_address
      user = var.admin_username
#      password = random_password.this.result
      private_key = "~/.ssh/id_rsa"
    }
  }
}
```

### local-exec
It invokes a script/command locally after resource is created.
```terraform
resource "azurerm_virtual_machine" "this" {
  #...
  
  provisioner "local-exec" {
    command = "echo 'VM has been created'; ansible -i inventory.yaml -f playbook.yaml"
  }
}
```

### remote-exec
It invokes a script/command remotely after resource is created.
```terraform
resource "azurerm_virtual_machine" "this" {
  #...
  
  provisioner "remote-exec" {
    inline = [
    "cat /etc/nginx/nginx.conf",
    "/opt/install.sh"  
    ]
    
    connection {
      type = "ssh/winrm"
      host = azurerm_public_ip.this.ip_address
      user = var.admin_username
    # password = random_password.this.result
      private_key = "~/.ssh/id_rsa"
    }
  }
}
```

## How to automatically generate terraform documentation?
- https://terraform-docs.io/user-guide/introduction/
- Commands
    ```bash
      $ terraform-docs markdown document . > README_DOC.md

      $ terraform-docs markdown table . > README_TABLE.md
    ```
----------------------------------------------------------------

## Demos
- [demo-001](./demo-001) - `azurerm_subscription` data 
- [demo-002](./demo-002) - `publicip` provider
- [demo-003](./demo-003) - `time` provider
- [demo-004](./demo-004) - Provisioners example

## Reference docs
- https://developer.hashicorp.com/terraform/language/resources/provisioners/syntax
