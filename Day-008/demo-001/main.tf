provider "azurerm" {
  features {}
}

data "azurerm_subscription" "current" {
  subscription_id = "df3cad63-87c7-4e3c-b4b8-12e0be25a943"
}

output "tenant_id" {
  value = data.azurerm_subscription.current.tenant_id
}

output "display_name" {
  value = data.azurerm_subscription.current.display_name
}

output "spending_limit" {
  value = data.azurerm_subscription.current.spending_limit
}