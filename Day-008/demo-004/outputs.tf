output "public_ip" {
  description = "Public ip address of the vm"
  value       = azurerm_public_ip.this.ip_address
}

output "private_key" {
  description = "Private key of the vm"
  value       = tls_private_key.this.private_key_pem
  sensitive   = true
}