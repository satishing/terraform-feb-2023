## Requirements

The following requirements are needed by this module:

- <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) (~> 1.3.9)

- <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) (~> 3.47.0)

- <a name="requirement_publicip"></a> [publicip](#requirement\_publicip) (~> 0.0.9)

- <a name="requirement_time"></a> [time](#requirement\_time) (~> 0.9.1)

- <a name="requirement_tls"></a> [tls](#requirement\_tls) (~> 4.0.4)

## Providers

The following providers are used by this module:

- <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) (3.47.0)

- <a name="provider_publicip"></a> [publicip](#provider\_publicip) (0.0.9)

- <a name="provider_time"></a> [time](#provider\_time) (0.9.1)

- <a name="provider_tls"></a> [tls](#provider\_tls) (4.0.4)

## Modules

No modules.

## Resources

The following resources are used by this module:

- [azurerm_network_interface.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_interface) (resource)
- [azurerm_network_interface_security_group_association.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_interface_security_group_association) (resource)
- [azurerm_network_security_group.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_security_group) (resource)
- [azurerm_public_ip.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/public_ip) (resource)
- [azurerm_resource_group.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group) (resource)
- [azurerm_subnet.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet) (resource)
- [azurerm_virtual_machine.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_machine) (resource)
- [azurerm_virtual_network.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_network) (resource)
- [time_static.this](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/static) (resource)
- [tls_private_key.this](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key) (resource)
- [publicip_address.this](https://registry.terraform.io/providers/nxt-engineering/publicip/latest/docs/data-sources/address) (data source)

## Required Inputs

The following input variables are required:

### <a name="input_address_prefixes"></a> [address\_prefixes](#input\_address\_prefixes)

Description: Address prefixes for subnet

Type: `list(string)`

### <a name="input_address_space"></a> [address\_space](#input\_address\_space)

Description: Address space for the virtual network

Type: `list(string)`

### <a name="input_allocation_method"></a> [allocation\_method](#input\_allocation\_method)

Description: Public IP allocation method, `Static` or `Dynamic`

Type: `string`

### <a name="input_location"></a> [location](#input\_location)

Description: Location for the resources.

Type: `string`

### <a name="input_os_profile"></a> [os\_profile](#input\_os\_profile)

Description: Virtual machine os profile configuration block

Type:

```hcl
object({
    admin_username = string
    computer_name  = string
  })
```

### <a name="input_os_profile_linux_config"></a> [os\_profile\_linux\_config](#input\_os\_profile\_linux\_config)

Description: Virtual machine os profile configuration block configuration

Type:

```hcl
object({
    disable_password_authentication = string
  })
```

### <a name="input_prefix"></a> [prefix](#input\_prefix)

Description: Prefix for the resource to be created

Type: `string`

### <a name="input_private_ip_address_allocation"></a> [private\_ip\_address\_allocation](#input\_private\_ip\_address\_allocation)

Description: Private IP allocation method, `Static` or `Dynamic`

Type: `string`

### <a name="input_storage_image_reference"></a> [storage\_image\_reference](#input\_storage\_image\_reference)

Description: Virtual machine storage image reference configuration block

Type:

```hcl
object({
    publisher = string
    offer     = string
    sku       = string
    version   = string
  })
```

### <a name="input_storage_os_disk"></a> [storage\_os\_disk](#input\_storage\_os\_disk)

Description: Virtual machine storage os disk configuration block

Type:

```hcl
object({
    name              = string
    caching           = string
    create_option     = string
    managed_disk_type = string
  })
```

### <a name="input_vm_size"></a> [vm\_size](#input\_vm\_size)

Description: Virtual machine size

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### <a name="input_tags"></a> [tags](#input\_tags)

Description: Tags for the resources.

Type: `map(any)`

Default: `{}`

## Outputs

The following outputs are exported:

### <a name="output_private_key"></a> [private\_key](#output\_private\_key)

Description: Private key of the vm

### <a name="output_public_ip"></a> [public\_ip](#output\_public\_ip)

Description: Public ip address of the vm
