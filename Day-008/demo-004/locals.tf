locals {
  tags = {
    "Business Unit"      = "Operations"
    "Cost Center"        = "CC-IDC-001"
    "Created By"         = "John.Doe@example.com"
    "Managed By"         = "IT Operations"
    "Owner"              = "Bob.Andrew@example.com"
    "Technical Owner"    = "Vikas.Marathe@example.com"
    "Environment"        = "Development"
    "Data Type"          = "Non-critical/Internal"
    "Project"            = "alpha"
    "Creation Timestamp" = time_static.this.id
  }
}