#!/usr/bin/env bash

sudo apt-get update -y
sudo apt-get install apache2 -y
sudo systemctl enable apache2
system restart apache2

echo "<h1>Hi there,</h1><p>This web page is deployed via terraform using provisioners</p>" | sudo tee /var/www/html/index.html