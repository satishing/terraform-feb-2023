provider "azurerm" {
  features {}
}

provider "tls" {}

provider "time" {}

provider "publicip" {
  provider_url = "https://ifconfig.co/" # optional
  timeout      = "10s"                  # optional

  # 1 request per 500ms
  rate_limit_rate  = "500ms" # optional
  rate_limit_burst = "1"     # optional
}

terraform {
  required_version = "~> 1.3.9"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.47.0"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0.4"
    }

    time = {
      source  = "hashicorp/time"
      version = "~> 0.9.1"
    }

    publicip = {
      source  = "nxt-engineering/publicip"
      version = "~> 0.0.9"
    }
  }
}