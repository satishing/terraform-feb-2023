variable "prefix" {
  description = "Prefix for the resource to be created"
  type        = string
}

variable "location" {
  description = "Location for the resources."
  type        = string
}

variable "tags" {
  description = "Tags for the resources."
  type        = map(any)
  default     = {}
}

variable "address_space" {
  description = "Address space for the virtual network"
  type        = list(string)
}

variable "address_prefixes" {
  description = "Address prefixes for subnet"
  type        = list(string)
}

variable "allocation_method" {
  description = "Public IP allocation method, `Static` or `Dynamic`"
  type        = string
}

variable "private_ip_address_allocation" {
  description = "Private IP allocation method, `Static` or `Dynamic`"
  type        = string
}

variable "vm_size" {
  description = "Virtual machine size"
  type        = string
}

variable "storage_os_disk" {
  description = "Virtual machine storage os disk configuration block"
  type = object({
    name              = string
    caching           = string
    create_option     = string
    managed_disk_type = string
  })
}

variable "storage_image_reference" {
  description = "Virtual machine storage image reference configuration block"
  type = object({
    publisher = string
    offer     = string
    sku       = string
    version   = string
  })
}

variable "os_profile" {
  description = "Virtual machine os profile configuration block"
  type = object({
    admin_username = string
    computer_name  = string
  })
}

variable "os_profile_linux_config" {
  description = "Virtual machine os profile configuration block configuration"
  type = object({
    disable_password_authentication = bool
  })
}
