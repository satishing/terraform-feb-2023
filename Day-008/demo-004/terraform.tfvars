prefix           = "alpha"
location         = "westeurope"
address_space    = ["10.0.0.0/16"]
address_prefixes = ["10.0.0.0/24"]

allocation_method             = "Static"
private_ip_address_allocation = "Dynamic"

vm_size = "Standard_B2s"

storage_image_reference = {
  publisher = "Canonical"
  offer     = "UbuntuServer"
  sku       = "16.04-LTS"
  version   = "latest"
}

storage_os_disk = {
  name              = "alpha-os-disk"
  caching           = "ReadWrite"
  create_option     = "FromImage"
  managed_disk_type = "Standard_LRS"
}

os_profile = {
  admin_username = "systemadmin"
  computer_name  = "example.com"
}

os_profile_linux_config = {
  disable_password_authentication = true
}

tags = {
  "Provisioned With" = "Terraform"
}