# Session - 001


## Challenges with traditional infrastructure management 
- Limited scale of deployments
- Poor scalability and elasticity of the infrastructure
- Extremely low cost-optimization levels
- Instances were too large and not cost-effective
- The human error element was omnipresent
- Major difficulties with configuration drifts and the management of vast infrastructures
- Slow provisioning, etc.

## Infrastructure as  a Code (IaC)
- It is an IT practice which manages/provisions IT infrastructure using a programming/code.
- Basically, **Infrastructure as Code is a model that codifies everything.**

### Types IaC 
- **Imperative** – This approach focuses on how and requires you to list the commands you want to be run in order to create the right resources.
    
    Example -

    `az group create --location westeurope --name my-resourcegroup`

- **Declarative** – This type of IaC allows you to say what you want to happen and the system will figure out the steps needed to achieve those results.

    Example -
  ```
  resource "azurerm_resource_group" "my-resourcegroup" {
      name = "my-resourcegroup"
      location = "westeurope"
  }
  ```

### IaC Tools
- Terraform
- Pulumi
- CFN
- AzureRM
- Ansible
- Puppet

### Why IaC ?
- To reduce the errors
- Reduce manual efforts
- Eliminate configuration drift
- Cost reduction
- Increase the speed of deployments
- Improve infrastructure consistency
----------------------------------------------------------------
# TERRAFORM
- Open source tool created by Hashicorp
- It is a JSON like configuration language created by Hashicorp using go language called HCL (Hashicorp configuration language)
which manages /provisions infrastructure across multiple platforms/clouds
- Hashicorp has three Terraform editions
  - **Terraform Open Source Software (OSS)**
  - **Terraform Enterprise** - Terraform Enterprise runs a private implementation that is deployed on Linux machines either on-premises or in your public cloud providers deployed instances.
  - **Terraform Cloud** - Terraform Cloud is HashiCorp’s SaaS-based version of Terraform that runs on disposable virtual machine instances that are deployed in their own Cloud infrastructure
  
  Refer: [Terraform editions explained](https://amazic.com/terraform-editions-explained-cloud-enterprise-and-oss/)

### Core components of terraform
Terraform is logically split into two main parts: Terraform Core and Terraform Plugins. Terraform Core uses remote procedure calls (RPC) to communicate with Terraform Plugins, and offers multiple ways to discover and load plugins to use. Terraform Plugins expose an implementation for a specific service, such as AWS, or provisioner, such as bash
#### 1. Terraform Core
Terraform Core is a statically-compiled binary written in the Go programming language. The compiled binary is the command line tool (CLI) `terraform`

The primary responsibilities of Terraform Core are:
- Infrastructure as code: reading and interpolating configuration files and modules
- Resource state management
- Construction of the Resource Graph
- Plan execution
- Communication with plugins over RPC

#### 2. Terraform Plugins
Terraform Plugins are written in Go and are executable binaries invoked by Terraform Core over RPC. Each plugin exposes an implementation for a specific service, such as AWS, or provisioner, such as bash. All Providers and Provisioners used in Terraform configurations are plugins.

The primary responsibilities of Provider Plugins are:
- Initialization of any included libraries used to make API calls
- Authentication with the Infrastructure Provider
- Define Resources that map to specific Services

### What is an API in brief ? 
- API acronym Application Programming Interface
- It is a software/program that sends and receives info/data back and forth between applications/webs/users
- Inorder to communicate between two systems API requires Authentication, Authorization
- Types of API protocols
  - **REST - Representational State Transfer**
     - REST APIs can only transmit information through the HTTP protocol
     - Data transfer happens generally using text or JSON format.
  - **SOAP - Simple Object Access Protocol**
    - A SOAP API can communicate over other major internet communication protocols, such as TCP and SMTP, in addition to HTTP
    - SOAP APIs can only work with XML data and have much more rigid requirements for requests.

### What is Terraform used for?
- External resource management (Public/Private Clouds / SaaS)
- Multi cloud deployment 
- Multi tier apps
- Self service for disposable environments
- SDN - Software Defined Network

### Disadvantages/Limitations of terraform
- New feature / enhancements may take some time to come to plugins
- New release may have bugs
- Breaking changes
- HCL is new language to learn
- No support for error handling in language
- Renaming existing resource, importing existing resource is difficult or somtimes it recreated resource

### Azure ARM vs Terraform
#### ARM
 - Advantages:
   - Native, always get support
   - Updates are on time
   - Faster
 - Disadvantages:
   - json - templates are heavy and gets complicated  
   - Doesn't have state of actual infrastructure

#### Terraform:
  - Advantages:
    - state of actual infrastructure
    - Easy to write and maintain
    - You can write reusable code
    - Provides language features such as conditions, loops, functions
    - Multi cloud support

## Demos
- [demo-001](./demo-001) - Basic example

