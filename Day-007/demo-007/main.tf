provider "azurerm" {
  features {}
}

variable "rg_names" {
  default = ["alpha", "beta", "charlie", "delta", "gamma"]
}

resource "azurerm_resource_group" "this" {
  for_each = toset(var.rg_names)
  name     = each.key
  location = "eastus"
}
