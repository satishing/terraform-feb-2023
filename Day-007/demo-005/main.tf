provider "azurerm" {
  features {}
}

variable "rg_names" {
  default = ["alpha", "beta", "charlie", "delta", "gamma"]
}

resource "azurerm_resource_group" "this" {
  count    = length(var.rg_names)
  name     = var.rg_names[count.index]
  location = "eastus"
}
