provider "azurerm" {
  features {}
}

variable "resource_group_name" {}

data "azurerm_resource_group" "this" {
  name = var.resource_group_name
}

resource "azurerm_storage_account" "this" {
  name                     = "somestorageaccount"
  resource_group_name      = data.azurerm_resource_group.this.name
  location                 = data.azurerm_resource_group.this.location
  account_replication_type = "LRS"
  account_tier             = "Standard"

  lifecycle {
    create_before_destroy = true
  }
}