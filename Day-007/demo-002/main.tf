provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "this" {
  location = "rg-alpha"
  name     = "westeurope"
}

resource "azurerm_virtual_network" "this" {
  name                = "vnet-alpha"
  address_space       = ["10.0.0.0/16"]
  location            = "westeurope"
  resource_group_name = "rg-alpha"

  depends_on = [azurerm_resource_group.this]
}