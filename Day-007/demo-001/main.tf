variable "foo_client_id" {}
variable "foo_client_secret" {}
variable "foo_tenant_id" {}

# Provider block
provider "azurerm" {
  alias           = "foo"
  client_id       = var.foo_client_id     # $Env:TF_VAR_foo_client_id = ""     # export TF_VAR_foo_client_id=""
  client_secret   = var.foo_client_secret # $Env:TF_VAR_foo_client_secret = "" # export TF_VAR_foo_client_secret=""
  subscription_id = "subscription_foo"
  tenant_id       = var.foo_tenant_id
  features {}
}

variable "bar_client_id" {}
variable "bar_client_secret" {}
variable "bar_tenant_id" {}

# Provider block
provider "azurerm" {
  alias           = "bar"
  client_id       = var.bar_client_id     # $Env:TF_VAR_bar_client_id = ""
  client_secret   = var.bar_client_secret # $Env:TF_VAR_bar_client_secret = ""
  subscription_id = "subscription_bar"
  tenant_id       = var.bar_tenant_id
  features {}
}

resource "azurerm_storage_account" "this" {
  provider = "azurerm.foo" # Meta argument

  account_replication_type = "LRS"
  account_tier             = "Standard"
  location                 = "eastus"
  name                     = "foosomestorageaccount"
  resource_group_name      = "rg-alpha"
}

resource "azurerm_storage_account" "this" {
  provider = "azurerm.bar" # Meta argument

  account_replication_type = "LRS"
  account_tier             = "Standard"
  location                 = "eastus"
  name                     = "barsomestorageaccount"
  resource_group_name      = "rg-alpha"
}