provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "this" {
  count    = 5
  name     = "rg-alpha-${count.index}"
  location = "eastus"
}