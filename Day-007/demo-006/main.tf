provider "azurerm" {
  features {}
}

variable "group_info" {
  default = {
    "rg-alpha-eastus"        = "eastus"
    "rg-beta-westeurope"     = "westeurope"
    "rg-charlie-taiwannorth" = "taiwannorth"
  }
}

resource "azurerm_resource_group" "this" {
  for_each = var.group_info
  name     = each.key
  location = each.value
}
