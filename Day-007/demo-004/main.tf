provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "this" {
  name     = "rg-alpha"
  location = "eastus"
}

variable "subnet_names" {
  default = ["dmz", "private", "public", "data"]
}

variable "subnet_prefixes" {
  default = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

resource "azurerm_virtual_network" "this" {
  name                = "vnet-alpha"
  resource_group_name = azurerm_resource_group.this.name
  location            = azurerm_resource_group.this.location
  address_space       = ["10.0.0.0/22"]
}

resource "azurerm_subnet" "this" {
  count                = length(var.subnet_names) # 4
  name                 = var.subnet_names[count.index]
  address_prefixes     = [var.subnet_prefixes[count.index]]
  resource_group_name  = azurerm_resource_group.this.name
  virtual_network_name = azurerm_virtual_network.this.name
}