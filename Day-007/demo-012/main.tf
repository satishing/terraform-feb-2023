provider "azurerm" {
  features {}
}

variable "tags" {
  default = {}
}

resource "azurerm_resource_group" "this" {
  location = "eastus"
  name     = "rg-alpha"
  tags = merge(
    var.tags, {
      "owner" = "Sam"
    }
  )

  lifecycle {
    ignore_changes = [tags]
  }
}

