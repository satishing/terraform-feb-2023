provider "azurerm" {
  features {}
}

variable "resource_group_name" {}
variable "location" {}
variable "address_space" {}
variable "vnet_name" {}

resource "azurerm_resource_group" "this" {
  name     = var.resource_group_name
  location = var.location
}

resource "azurerm_virtual_network" "this" {
  address_space       = var.address_space
  location            = azurerm_resource_group.this.location
  name                = var.vnet_name
  resource_group_name = azurerm_resource_group.this.name
}

output "resource_group_id" {
  value = azurerm_resource_group.this.id
}

output "vnet_id" {
  value = azurerm_virtual_network.this.id
}