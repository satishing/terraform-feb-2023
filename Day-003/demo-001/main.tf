# This is an example for comments in terraform
# This is a provider block
provider "azurerm" {
  features {
  }
}

// This is azure resource group block
resource "azurerm_resource_group" "this" {
  location = "westeurope"
  name     = "rg-project-beta"
}

/*
Below block is for virtual network where
cidr is 10.0.0.0/18
location is same as resource group location
*/

resource "azurerm_virtual_network" "this" {
  address_space       = ["10.0.0.0/18"]
  location            = "westeurope"
  name                = "vnet-project-beta"
  resource_group_name = "rg-project-beta"
}

/*This is end of configuration*/