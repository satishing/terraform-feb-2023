# Session - 003

## Language basics
Terraform used HCL(Hashicorp Configuration Language) to declare resources, which represent infrastructure objects. 

A Terraform configuration is a complete document in the Terraform language that tells Terraform how to manage a given collection of infrastructure. A configuration can consist of multiple files and directories.

The syntax of the Terraform language consists of only a few basic elements:
### Blocks
Blocks are containers for other content and usually represent the configuration of some kind of object, like a resource.

Blocks have a block type, can have zero or more labels, and have a body that contains any number of arguments and nested blocks.
### Arguments
Arguments assign a value to a name. They appear within blocks.
### Expressions
Expressions represent a value, either literally or by referencing and combining other values. They appear as values for arguments, or within other expressions.

Terraform syntax looks like below
```terraform
<block_type> "<block_label>" "<block_label>" {
  # Block body
  <identifier> = <value> # Argument
  <identifier> = <expression> # Argument
}
```
Example.
```terraform
provider "azurerm" {
  features {} # block with no labels
  environment = "public" # Argument
}

resource "azurerm_resource_group" "example" {
  name      = "rg-project-alpha"
  locations = "westeurope"
  tags      = {
    Environment = "development"
  }
}

resource "aws_instance" "this" {
  ami = "amzn_linux_x64"

  network_interface {
    device_index         = 0
    network_interface_id = ""
  }
  ...
}
```

## Terraform configuration language syntax
1. HCL `main.tf`
```terraform
resource "aws_instance" "this" {
  instance_type = "t2.micro"
  ami = "ami-abc575gi7uh"
}
```
2. JSON
`main.tf.json`
```json
{
  "resource": {
    "aws_instance": {
      "this": {
        "instance_type": "t2.micro",
        "ami": "ami-abc575gi7uh"
      }
    }
  }
}

```

## Terraform Core workflow
```shell
terraform init
terraform validate
terraform fmt
terraform plan
terraform apply
terraform destroy
```

## Variables in terraform
Input variables let you customize aspects of Terraform modules without altering the module's own source code. This functionality allows you to share modules across different Terraform configurations, making your module composable and reusable.

### Different methods to provide values for variable in terraform

- [Interactive mode](#interactive-mode)
- [Using variable defaults](#)
- [Using terraform commandline argument `--var`](#using-terraform-commandline-argument-`--var`)
- [Using terraform commandline argument `--var-file`](#using-terraform-commandline-argument-`--var-file`)
- [Using environment variable `TF_VAR_<variable_name>`](#using-environment-variable-`TF_VAR_<variable_name>`)


### Interactive mode
```terraform
variable "resource_group_name" {}
variable "location" {}

resource "azurerm_resource_group" "example" {
  name     = var.resource_group_name
  location = var.location
}
```
If you do not provide value to variables in any way, terraform will ask you to input variable value.
```bash
$ terraform plan
var.location
  Enter a value: 

```

### Using variable defaults
```terraform
variable "resource_group_name" {
  default = "rg-alpha"
}
variable "location" {
  default = "westeurope"
}

resource "azurerm_resource_group" "example" {
  name     = var.resource_group_name
  location = var.location
}
```

### Using terraform commandline argument `--var`
You can override the defaults by providing values for the variables during the plan and apply.

```bash
$ terraform plan --var resource_group_name=rg-charlie --var location=eastus

$ terraform apply --var resource_group_name=rg-charlie --var location=eastus
```

### Using terraform commandline argument `--var-file`
- if you are using any other file name than `terraform.tfvars` then you need to provide `--var-file` option to `terraform plan` and `terraform apply` command.

```bash
$ terraform plan --var dev.tfvars

$ terraform apply --var-file dev.tfvars 
```
Variable helps in achieving code re-usability.
for example if you have the following requests from different teams to create same infrastructure. You can create different `<env>.tfvars` file without changing code.
```bash
# Application Team - Pls provide a resource group for the application deployment for Dev Environment
$ terraform plan --var-file dev.tfvars

# Testing Team     - Pls provide a resource group for the application deployment for QA Environment
$ terraform plan --var-file qa.tfvars

# Integration Team - Pls provide a resource group for the application deployment for Integ Environment
$ terraform plan --var-file integ.tfvars

# PreProd          - Pls provide a resource group for the application deployment for ProProd Environment
$ terraform plan --var-file preprod.tfvars

# Prod             - Pls provide a resource group for the application deployment for Prod Environment
$ terraform plan --var-file prod.tfvars
```

### Using environment variable `TF_VAR_<variable_name>`

If you want to provide the values of variable using the environment variable the use as below for give example.

bash
```bash
export TF_VAR_azurerm_resource_group="rg-alpha"
export TF_VAR_location="eastus"
```
cmd
```cmd
set TF_VAR_azurerm_resource_group="rg-alpha"
set TF_VAR_location="eastus"
```

## Demos
- [demo-001](./demo-001) - Terraform language syntax and comments
- [demo-002](./demo-002) - Terraform HCL and JSON syntax examples
- [demo-003](./demo-003) - Logical representation how to use different `tfvars` files for code reuse. Will be covered in detail in `workspaces` section
- [demo-004](./demo-004) - Variables/Outputs example

## Reference docs
- https://developer.hashicorp.com/terraform/language
- https://developer.hashicorp.com/terraform/language/values