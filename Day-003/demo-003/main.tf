provider "azurerm" {
  features {}
}

variable "resource_group_name" {}
variable "resource_group_location" {}
variable "tags" {
  default = {
    CreatedBy = "Terraform"
  }
}

resource "azurerm_resource_group" "example" {
  name     = var.resource_group_name
  location = var.resource_group_location
  tags     = var.tags
}

