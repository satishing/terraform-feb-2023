provider "time" {}
resource "time_static" "current" {}

output "timestamp" {
  value = formatdate("EEEE, DD MMM YYYY hh:mm ZZZ", time_static.current.id)
}