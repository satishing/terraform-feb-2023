# Session - 009

# Topics

- [Operators](#operators)
- [Functions](#functions)
- [Type Constraints](#type-constraints)
- [Version Constraints](#version-constraints)

--------------------------------------------------------------------------------

## Operators
| Operator Type | Operators                 |   
|---------------|---------------------------|
| Arithmetic    | `+`, `-`, `*`, `/`, `%`   |
| Equality      | `==`, `!=`                |
| Comparison    | `<`, `<=`, `>`, `>=`      |
| Logical       | `&&`, `!`, `&#124;&#124;` |                

--------------------------------------------------------------------------------

## Functions
The Terraform language supports only the functions built in to the language are available for use.
You can experiment with the behavior of Terraform's built-in functions from the Terraform expression console, by running the `terraform console` command:

| Numeric  | String     | Collection      | Encoding         | Filesystem   | Date & Time | Hash & Crypto    | IP Network  | Type Conversion |
|----------|------------|-----------------|------------------|--------------|-------------|------------------|-------------|-----------------|
| abs      | chomp      | alltrue         | base64decode     | abspath      | formatdate  | base64sha256     | cidrhost    | can             |
| ceil     | endswith   | anytrue         | base64encode     | dirname      | timeadd     | base64sha512     | cidrnetmask | nonsensitive    |
| floor    | format     | chunklist       | base64zip        | pathexpand   | timecmp     | bcrypt           | cidrsubnet  | sensitive       |
| log      | formatlist | coalesce        | csvdecode        | basename     | timestamp   | filebase64sha256 | cidrsubnets | tobool          |
| max      | indent     | coalescelist    | jsondecode       | file         |             | filebase64sha512 |             | tolist          |
| min      | join       | compact         | jsobencode       | filexists    |             | filemd5          |             | tomap           |
| parseint | lower      | contains        | textdecodebase64 | fileset      |             | filesha1         |             | tonumber        |
| pow      | regex      | element         | textencodebase64 | filebase64   |             | filesha256       |             | toset           |
| signum   | regexall   | flatten         | urlencode        | templatefile |             | filesha512       |             | tostring        |
|          | replace    | index           | yamldecode       |              |             | md5              |             | try             |
|          | split      | keys            | yamlencode       |              |             | rsadecrypt       |             | type            |
|          | startswith | length          |                  |              |             | sha1             |             |                 |
|          | strrev     | list            |                  |              |             | sha256           |             |                 |
|          | substr     | lookup          |                  |              |             | sha512           |             |                 |
|          | title      | map             |                  |              |             | uuid             |             |                 |
|          | trim       | matchkeys       |                  |              |             | uuidv5           |             |                 |
|          | trimprefix | merge           |                  |              |             |                  |             |                 |
|          | trimsuffix | one             |                  |              |             |                  |             |                 |
|          | trimspace  | range           |                  |              |             |                  |             |                 |
|          | upper      | reverse         |                  |              |             |                  |             |                 |
|          |            | setintersection |                  |              |             |                  |             |                 |
|          |            | setproduct      |                  |              |             |                  |             |                 |
|          |            | setsubtract     |                  |              |             |                  |             |                 |
|          |            | setunion        |                  |              |             |                  |             |                 |
|          |            | slice           |                  |              |             |                  |             |                 |
|          |            | sort            |                  |              |             |                  |             |                 |
|          |            | sum             |                  |              |             |                  |             |                 |
|          |            | transpose       |                  |              |             |                  |             |                 |
|          |            | values          |                  |              |             |                  |             |                 |
|          |            | zipmap          |                  |              |             |                  |             |                 |
--------------------------------------------------------------------------------

## Type Constraints
Type constraints are used to validate user-provided values for their input variables and resource arguments.
- ### Primitive Types
  `string`, `number` and `bool`
- ### Complex Types
  `list` and `map`
- ### Structural Types
  `object` and `tuple`
- ### Dynamic Types
  `any`

--------------------------------------------------------------------------------

## Version Constraints
Terraform lets you specify a range of acceptable versions for **Modules**, **Provider requirements** and `required_version` setting in to **terraform block**.
Example
```terraform
version = ">= 1.2.0, < 2.0.0"
```
The following operators are valid:

`=`: (or no operator): Allows only one exact version number. Cannot be combined with other conditions.

`!=`: Excludes an exact version number.

`>, >=, <, <=`: Comparisons against a specified version, allowing versions for which the comparison is true. "Greater-than" requests newer versions, and "less-than" requests older versions.

`~>`: Allows only the rightmost version component to increment. For example, to allow new patch releases within a specific minor release, use the full version number ~> 1.0.4 will allow installation of 1.0.5 and 1.0.10 but not 1.1.0. This is usually called the pessimistic constraint operator.

----------------------------------------------------------------

## Demos
- [demo-001](./demo-001) - formatdate function

## Reference docs
- https://developer.hashicorp.com/terraform/language/functions