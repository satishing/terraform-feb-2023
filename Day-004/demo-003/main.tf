provider "local" {}

resource "local_file" "this" {
  filename        = "/tmp/sample.txt"
  content         = "This file is created by local provider using terraform."
  file_permission = "0600"
}
