provider "random" {}
provider "local" {}
provider "azurerm" {
  features {}
}

resource "random_password" "this" {
  length           = 12
  lower            = true
  min_lower        = 3
  upper            = true
  min_upper        = 3
  numeric          = true
  special          = true
  override_special = "$#_!"
}

# Example to randomly generated password in a file
resource "local_sensitive_file" "this" {
  filename = "password.txt"
  content  = random_password.this.result
}

# Example to store randomly generated password in azure key vault as secret
resource "azurerm_key_vault_secret" "this" {
  key_vault_id = ""
  name         = ""
  value        = random_password.this.result
  content_type = "password"
  expiration_date = "2023-12-31T00:00:00Z"
}


# Example to use random storage account name
resource "random_string" "example" {
  length = 16
}

resource "azurerm_storage_account" "example" {
  account_replication_type = ""
  account_tier             = ""
  location                 = ""
  name                     = random_string.example.result
  resource_group_name      = ""
  min_tls_version          = "TLS1_2"
  queue_properties  {
    logging {
      delete                = true
      read                  = true
      write                 = true
      version               = "1.0"
      retention_policy_days = 10
    }
  }
}
