# How to run this code ?
- Development environment
```shell
terraform workspace new dev
terraform workspace list
terraform workspace select dev
tterraform workspace show
terrafoirm init
terrafoirm validate
terrafoirm fmt
terrafoirm plan --var-file dev.tfvars
terrafoirm apply --var-file dev.tfvars
terrafoirm destroy --var-file dev.tfvars
```

- Testing environment
```shell
terraform workspace new test
terraform workspace list
terraform workspace select test
tterraform workspace show
terrafoirm init
terrafoirm validate
terrafoirm fmt
terrafoirm plan --var-file test.tfvars
terrafoirm apply --var-file test.tfvars
terrafoirm destroy --var-file test.tfvars
```

- Production environment
```shell
terraform workspace new prod
terraform workspace list
terraform workspace select prod
tterraform workspace show
terrafoirm init
terrafoirm validate
terrafoirm fmt
terrafoirm plan --var-file prod.tfvars
terrafoirm apply --var-file prod.tfvars
terrafoirm destroy --var-file prod.tfvars
```