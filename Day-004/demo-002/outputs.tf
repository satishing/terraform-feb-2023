# output
output "vnet_id" {
  description = "The virtual NetworkConfiguration ID"
  value       = azurerm_virtual_network.example.id
}

output "subnet" {
  description = "One or more subnet blocks as defined"
  value       = azurerm_virtual_network.example.subnet
}