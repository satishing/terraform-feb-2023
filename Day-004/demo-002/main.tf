# Resources
resource "azurerm_resource_group" "example" {
  name     = var.resource_group_name
  location = var.location
  tags     = local.tags
}

resource "azurerm_network_security_group" "example" {
  name                = var.security_group_name
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  tags                = local.tags
}

resource "azurerm_virtual_network" "example" {
  name                = var.virtual_network_name
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  address_space       = var.address_space
  dns_servers         = var.dns_servers

  subnet {
    name           = var.subnet_1_name
    address_prefix = var.subnet_1_address_prefix
  }

  subnet {
    name           = var.subnet_2_name
    address_prefix = var.subnet_2_address_prefix
    security_group = azurerm_network_security_group.example.id
  }

  flow_timeout_in_minutes = var.flow_timeout_in_minutes

  tags = local.tags
}


