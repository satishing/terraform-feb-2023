resource_group_name     = "rg-project-alpha-dev"
location                = "West Europe"
security_group_name     = "sg-project-alpha-dev"
virtual_network_name    = "vnet-project-alpha-dev"
address_space           = ["10.0.0.0/16"]
dns_servers             = ["10.0.0.4", "10.0.0.5"]
subnet_1_name           = "subnet1"
subnet_1_address_prefix = "10.0.1.0/24"
subnet_2_name           = "subnet2"
subnet_2_address_prefix = "10.0.2.0/24"