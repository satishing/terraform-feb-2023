# Variables
variable "resource_group_name" {
  description = "The name of the resource group in which to create the virtual network"
  type        = string
}

variable "location" {
  description = "The location/region where the virtual network is created"
  type        = string
}

variable "security_group_name" {
  description = "The name of the security group in which to create the virtual network"
  type        = string
}

variable "virtual_network_name" {
  description = "The name of the virtual network"
  type        = string
}

variable "address_space" {
  description = "The list of address spaces used by the virtual network."
  type        = list(any)
}

variable "dns_servers" {
  description = "The list of dns servers used by the virtual network"
  type        = list(string)
}

variable "subnet_1_address_prefix" {
  description = "The address prefix for subnet 1"
  type        = string
}

variable "subnet_2_address_prefix" {
  description = "The address prefix for subnet 2"
  type        = string
}

variable "subnet_1_name" {
  description = "The name for subnet 1"
  type        = string
}

variable "subnet_2_name" {
  description = "The name for subnet 1"
  type        = string
}

variable "flow_timeout_in_minutes" {
  description = "The flow timeout in minutes for the Virtual Network, which is used to enable connection tracking for intra-VM flows. Possible values are between 4 and 30 minutes."
  type        = number
  default     = 4
}