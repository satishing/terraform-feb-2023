# locals
locals {
  tags = {
    "Business unit"       = "Finance"
    "Operations team"     = "Avengers"
    "Data classification" = "Public"
    "Cost center"         = "55440987"
    "Environment"         = "prod"
  }
}