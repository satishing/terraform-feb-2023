# Providers
provider "azurerm" {
  features {}
}

# Variables
variable "resource_group_name" {
  description = "The name of the resource group in which to create the virtual network"
  type        = string
  default     = "example-resources"
}

variable "location" {
  description = "The location/region where the virtual network is created"
  type        = string
  default     = "West Europe"
}

variable "security_group_name" {
  description = "The name of the security group in which to create the virtual network"
  type        = string
  default     = "example-security-group"
}

variable "virtual_network_name" {
  description = "The name of the virtual network"
  type        = string
  default     = "example-network"
}

variable "address_space" {
  description = "The list of address spaces used by the virtual network."
  type        = list(any)
  default     = ["10.0.0.0/16"]
}

variable "dns_servers" {
  description = "The list of dns servers used by the virtual network"
  type        = list(string)
  default     = ["10.0.0.4", "10.0.0.5"]
}

variable "subnet_1_address_prefix" {
  description = "The address prefix for subnet 1"
  type        = string
  default     = "10.0.1.0/24"
}
variable "subnet_2_address_prefix" {
  description = "The address prefix for subnet 2"
  type        = string
  default     = "10.0.2.0/24"
}
variable "subnet_1_name" {
  description = "The name for subnet 1"
  type        = string
  default     = "subnet1"
}
variable "subnet_2_name" {
  description = "The name for subnet 1"
  type        = string
  default     = "subnet2"
}

# locals
locals {
  tags = {
    "Business unit"       = "Finance"
    "Operations team"     = "Avengers"
    "Data classification" = "Public"
    "Cost center"         = "55440987"
    "Environment"         = "prod"
  }
}


# Resources
resource "azurerm_resource_group" "example" {
  name     = var.resource_group_name
  location = var.location
  tags     = local.tags
}

resource "azurerm_network_security_group" "example" {
  name                = var.security_group_name
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  tags                = local.tags
}

resource "azurerm_virtual_network" "example" {
  name                = var.virtual_network_name
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  address_space       = var.address_space
  dns_servers         = var.dns_servers

  subnet {
    name           = var.subnet_1_name
    address_prefix = var.subnet_1_address_prefix
  }

  subnet {
    name           = var.subnet_2_name
    address_prefix = var.subnet_2_address_prefix
    security_group = azurerm_network_security_group.example.id
  }

  tags = local.tags
}

# output
output "vnet_id" {
  description = "The virtual NetworkConfiguration ID"
  value       = azurerm_virtual_network.example.id
}

output "subnet" {
  description = "One or more subnet blocks as defined"
  value       = azurerm_virtual_network.example.subnet
}