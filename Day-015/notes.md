# Session - 015

----------------------------------------------------------------
# Topics
- Terraform configuration scanning tools
- Miscellaneous


## Terraform configuration scanning tools
**Trivy** and **Terrascan** are two open source tools that can be used to scan Terraform code for vulnerabilities and potential security issues.

### Trivy:  
It is a container vulnerability scanner that can be used to scan Docker images for known vulnerabilities in OS packages and application dependencies. Trivy can also be used to scan the underlying base images used by a Docker container, which can help identify any vulnerabilities that may be present in those images.

In the context of Terraform, Trivy can be used to scan the Docker containers created by Terraform code for vulnerabilities. This is useful for identifying any security issues that may be present in the containers, which can help mitigate potential security risks.

### Terrascan:
It is a tool that can be used to scan Terraform code for security issues and policy violations. Terrascan has a set of built-in policies that can be used to identify common security issues and best practices violations in Terraform code. Terrascan can also be extended with custom policies to meet specific security requirements.

In the context of Terraform, Terrascan can be used to scan Terraform code for security issues and policy violations before deploying the infrastructure. This can help identify any security risks or policy violations early in the development process, which can help reduce the risk of security breaches or non-compliance issues.

Overall, Trivy and Terrascan are two tools that can be used to help ensure the security of Terraform code and the underlying infrastructure. By using these tools, you can identify and address security issues early in the development process, which can help reduce the risk of security breaches and other security-related incidents.


## Miscellaneous
### Terraform template
[demo-002](./demo-002): This Terraform code creates an AWS IAM policy that can contain custom policy statements provided by the user.

In the `data` block, it defines an empty AWS IAM policy document. This is used to extract the `Version` of the IAM policy document, which is required when creating a new IAM policy.

In the `variable` block, a variable called `custom_policy_statements` is defined, which is a list of policy statements that will be added to the custom IAM policy.

In the `resource` block, a new AWS IAM policy is created. The name of the policy is set to "my-policy". The `jsonencode` function is used to convert the list of policy statements provided in the `custom_policy_statements` variable into a JSON string.

The `templatefile` function is used to render the contents of `policy.tftpl` file. The `templatefile` function takes two arguments: the path to the template file, and a map of values to use when rendering the template. In this case, the values are a single key-value pair where the key is `statements` and the value is the `custom_policy_statements` variable defined earlier.

The `jsondecode` function is used to decode the resulting JSON string from the `templatefile` function and extract the `Statement` section from the policy document. The `Version` section is obtained from the empty IAM policy document defined earlier using `data.aws_iam_policy_document.this.json`.

Finally, the `aws_iam_policy` resource is created using the extracted `Version` and `Statement` sections to create a custom IAM policy with the specified name and policy statements.

Overall, this code allows the user to define custom policy statements and add them to a new IAM policy on AWS using Terraform.

### Terraform Taint
`terraform taint` is a command that is used to mark a specific resource managed by Terraform as **tainted** or **damaged**. This means that Terraform will destroy and recreate that resource on the next apply, even if there have been no changes to its configuration.

A common use case for tainting a resource is when you need to force Terraform to recreate a resource due to a problem or issue with it. For example, let's say you have created an Azure virtual machine using Terraform, and you notice that it is not functioning properly. You suspect that the issue may be due to a misconfiguration or some other issue with the resource.

To taint the resource using the Terraform command line, you can run the following command:

```terraform
terraform taint azurerm_virtual_machine.vm
```
This will mark the Azure virtual machine resource with the resource ID `vm` as tainted. The next time you run terraform apply, Terraform will destroy and recreate the virtual machine, even if there have been no changes to its configuration.

It's important to note that tainting a resource should be used with caution, as it can result in data loss or downtime if not used correctly. It's recommended to first investigate and troubleshoot the issue before tainting a resource, and to carefully consider the impact of tainting the resource on any dependent resources or infrastructure.

In summary, Terraform taint is a command that is used to mark a resource as tainted, which forces Terraform to destroy and recreate it on the next apply. It can be useful for troubleshooting and fixing issues with resources managed by Terraform, but should be used with caution to avoid unintended consequences.