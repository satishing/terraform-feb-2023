custom_policy_statements = [
  {
    actions = ["s3:Put*", "iam:ListRoles"]
    resources = ["*"]
  },
  {
    actions = ["eks:DescribeCluster"]
    resources = ["arn:aws:iam::038062473746:user/satishingale"]
  },
  {
    actions = ["ec2:*"]
    resources = ["*"]
  },
]