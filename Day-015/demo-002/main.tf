data "aws_iam_policy_document" "this" {}

variable "custom_policy_statements" {
  type  = list(any)
}

resource "aws_iam_policy" "custom_policy" {
  name = "my-policy"
  policy = jsonencode({
    Version = jsondecode(data.aws_iam_policy_document.this.json).Version
    Statement = jsondecode(templatefile("${path.module}/policy.tftpl", {
      statements = var.custom_policy_statements
    })).Statement
  })
}

