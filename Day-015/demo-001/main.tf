provider "aws" {
  region = "us-east-1"
}

resource "aws_iam_role" "example" {
  name = "example_role"
  description = "example role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = "sid1"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "example" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
  role       = aws_iam_role.example.name
}

resource "aws_iam_group" "example" {
  name = "devops-admin"
}

data "aws_iam_user" "existing"{
  user_name = "satishingale"
}

resource "aws_iam_group_membership" "example" {
  group = "devops-admin"
  name  = "devops-admin"
  users = [data.aws_iam_user.existing.user_name]
}

resource "aws_iam_policy" "example" {
  name = "example-policy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ec2:Describe*",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_policy_attachment" "example" {
  name       = "example_policy_attachment"
  policy_arn = aws_iam_policy.example.arn
  groups = [aws_iam_group.example.name]
}