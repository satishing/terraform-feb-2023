# Session - 013

----------------------------------------------------------------
# Topics
- Developing and publishing terraform modules

## Prerequisites
Before you start, you'll need to have the following:

- An Azure subscription
- Git installed on your machine
- Terraform installed on your machine
- An understanding of Terraform configuration files

## Creating the Terraform Module
1. Create a new directory for your Terraform module.
2. Inside the module directory, create a new file named `main.tf`. This file will contain the resources that you want to define in your module. For example, let's create a simple virtual network with a subnet:
```terraform
resource "azurerm_resource_group" "example" {
  name     = var.resource_group_name
  location = var.location
}

resource "azurerm_virtual_network" "example" {
  name                = var.virtual_network_name
  address_space       = var.virtual_network_address_space
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  subnet {
    name           = var.subnet_name
    address_prefix = var.subnet_address_prefix
  }
}
```
3. Define any variables you need for your module in `variables.tf`. Here's an example:
```terraform
variable "resource_group_name" {
  type        = string
  description = "Name of the resource group to create."
}

variable "location" {
  type        = string
  description = "Location of the resource group."
}

variable "virtual_network_name" {
  type        = string
  description = "Name of the virtual network to create."
}

variable "virtual_network_address_space" {
  type        = list(string)
  description = "Address space of the virtual network."
}

variable "subnet_name" {
  type        = string
  description = "Name of the subnet to create."
}

variable "subnet_address_prefix" {
  type        = string
  description = "Address prefix of the subnet."
}
```
4. Define any outputs you need for your module in `outputs.tf`. Here's an example:
```terraform
output "virtual_network_id" {
  value = azurerm_virtual_network.example.id
}

output "subnet_id" {
  value = azurerm_subnet.example.id
}
```
5. create `README.md` file for your module.
6. Test your module by running `terraform init` and `terraform apply` in the module directory. This will initialize Terraform and apply your module to your infrastructure.

## Publishing the Terraform Module on GitHub
- Create a new repository on GitHub for your Terraform module.
- Initialize a new Git repository in your module directory by running `git init`.
- Add your files to the Git repository by running `git add .`.
- Commit your changes by running `git commit -m "Initial commit"`.
- Add your GitHub repository as a remote by running `git remote add origin https://github.com/your-username/your-repo.git`.
- Create a tag for your module version by running `git tag -a v1.0.0 -m "Network module"`.
- Push your changes and tag to GitHub by running `git push -u origin main --tags`.

Your Terraform module is now published on GitHub! You can now use it as a remote module in your Terraform configurations.

## Using the Remote Terraform Module
1. In your Terraform configuration file, add a `module` block to reference the remote module. For example:
```terraform
module "vnet" {
source  = "github.com/your-username/your-repo?ref=v1.0.0"

resource_group_name        = "example-rg"
location                   = "eastus"
virtual_network_name       = "example-vnet"
virtual_network_address_space = ["10.0.0.0/16"]
subnet_name                = "example-subnet"
subnet_address_prefix      = "10.0.1.0/24"
}
```
Note that the `source` attribute specifies the URL of your GitHub repository, followed by the `ref` parameter specifying the tag of your module version.

2. Run `terraform init` to download the module from GitHub. Module will be downloaded to your `.terraform` directory.
3. Use the resources and outputs defined in the module in your configuration file. For example:
```terraform
resource "azurerm_resource_group" "example" {
  name     = var.resource_group_name
  location = var.location
}

module "vnet" {
  source  = "github.com/your-username/your-repo?ref=v1.0.0"

  resource_group_name        = azurerm_resource_group.example.name
  location                   = azurerm_resource_group.example.location
  virtual_network_name       = "example-vnet"
  virtual_network_address_space = ["10.0.0.0/16"]
  subnet_name                = "example-subnet"
  subnet_address_prefix      = "10.0.1.0/24"
}

output "vnet_id" {
  value = module.vnet.virtual_network_id
}

output "subnet_id" {
  value = module.vnet.subnet_id
}
```
4. Run `terraform apply` to create the resources defined in your configuration file.

And that's it! You now know how to create an Azure Terraform module, version it with Git tags, and use it as a remote module in your Terraform configurations.!!!
