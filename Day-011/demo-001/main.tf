provider "azurerm" {
  features {}
}

resource "azurerm_policy_definition" "policy" {
  name         = "accTestPolicy"
  policy_type  = "Custom"
  mode         = "Indexed"
  display_name = "acceptance test policy definition"

  metadata = <<METADATA
    {
    "category": "General"
    }
METADATA

  policy_rule = templatefile("${path.root}/policy.tftpl", { allowed_locations = ["westeurope"], effect = "allow" })

  parameters = <<PARAMETERS
 {
    "allowedLocations": {
      "type": "Array",
      "metadata": {
        "description": "The list of allowed locations for resources.",
        "displayName": "Allowed locations",
        "strongType": "location"
      }
    }
  }
PARAMETERS

}

#----------------------------------------------------------------
locals {
  custom_script = templatefile("${path.root}/script.sh", { environment = "Prod" })
}

resource "aws_instance" "this" {
  #...
  provisioner "remote-exec" {
    inline = ["/opt/script.sh"]
  }
}