provider "azurerm" {
  features {}
}

terraform {
  required_version = "~> 1.3.9"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.48.0"
    }
  }

  backend "azurerm" {
    resource_group_name  = "rg-project-alpha"
    storage_account_name = "myterraformremotestates"
    container_name       = "prod-tfstates"
    key_name             = "terraform.tfstate"

    encrypt {
      key_vault_id = "/subscriptions/xxxxxxxxx/resourceGroup/alpha/providers/Microsoft.KeyVault/vaults/my-vault"
      key_name     = "mykey"
    }
  }
}