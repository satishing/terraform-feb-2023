How to call azure apis?
Get the access token and subscription id.

token=$(az account get-access-token | jq -r ".accessToken")
subscriptionId=$(az account show | jq ".id" -r)


Azure api to get list of resource groups

curl -s -X GET -H "Authorization: Bearer ${token}" -H "Content-Type:application/json" -H "Accept:application/json" \
https://management.azure.com/subscriptions/${subscriptionId}/resourcegroups?api-version=2021-04-01 | jq -r ".value[].name"


Azure api to create a resource group

resourceGroupName=my-demo-rg

curl -i -X PUT -H "Authorization: Bearer ${token}" -H "Content-Type:application/json" -H "Accept:application/json" \
https://management.azure.com/subscriptions/${subscriptionId}/resourcegroups/${resourceGroupName}?api-version=2021-04-01 \
--data '{"location": "westeurope", "tags": {"CostCenter": "10000-12"}}'



Azure api to create a resource group

resourceGroupName=my-demo-rg

curl -i -X DELETE -H "Authorization: Bearer ${token}" -H "Content-Type:application/json" -H "Accept:application/json" \
https://management.azure.com/subscriptions/${subscriptionId}/resourcegroups/${resourceGroupName}?api-version=2021-04-01