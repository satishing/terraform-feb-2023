# Session - 012
# Topics

- [Terraform State Management](#terraform-state-management)
- [Workspaces](#workspaces)

## Terraform Workspaces
Terraform Workspaces allow you to maintain multiple instances of the same infrastructure with different configurations, without duplicating the code. With Terraform Workspaces, you can create multiple workspaces, each with its own state file, and switch between them easily.

### Creating a Workspace

To create a new workspace, use the terraform workspace new command:

```
$ terraform workspace new dev
```
This command will create a new workspace called `dev`. 

You can switch to this workspace using the terraform workspace select command:

```
$ terraform workspace select dev
```

### Using a Workspace
Once you have created a workspace, you can use it to deploy your infrastructure. When you run terraform apply, Terraform will use the state file associated with the current workspace. 

For example, if you have a workspace called `dev`, you can deploy your infrastructure to that workspace using the following command:

```
$ terraform apply -var-file=dev.tfvars
```
This command will use the state file associated with the `dev` workspace and the variables defined in the "dev.tfvars" file.

Example

Let's say you have two environments for your infrastructure: `dev` and `prod`. You can use Terraform Workspaces to maintain separate state files for each environment.

To create a new workspace for `prod`, use the following command:

```
$ terraform workspace new prod
```
To switch to the `prod` workspace, use the following command
```
$ terraform workspace select prod
```
Now, when you run terraform apply, Terraform will use the state file associated with the `prod` workspace.

## Terraform Import
Terraform Import is a command that allows you to import existing resources into your Terraform state file. With Terraform Import, you can bring existing resources under Terraform management without having to recreate them from scratch.

### Importing a Resource
To import a resource into your Terraform state file, you need to know the resource's ID. For example, let's say you want to import an existing AWS EC2 instance into your Terraform state file. You can use the following command:

```
$ terraform import aws_instance.this i-0123456789abcdef0
```
This command will import the EC2 instance with the ID "i-0123456789abcdef0" into your Terraform state file.

Example

Let's say you have an existing AWS S3 bucket that you want to manage with Terraform. You can use Terraform Import to bring this bucket under Terraform management.

First, create a new Terraform configuration file:

```
$ touch s3.tf
```
Next, add the following code to the s3.tf file:

```
resource "aws_s3_bucket" "this" {
    bucket = "my-existing-bucket"
}
```
Now, run the following command to import the existing S3 bucket:

```
$ terraform import aws_s3_bucket.this my-existing-bucket
```
This command will import the existing S3 bucket with the name "my-existing-bucket" into your Terraform state file. You can now use Terraform to manage this bucket just like any other resource.

----------------------------------------------------------------

## Demos
- [demo-001](./demo-001) - Terraform backends and workspaces
- [demo-002](./demo-002) - terraform import example

## Reference docs
https://www.terraform.io/language/state/workspaces#workspaces
https://developer.hashicorp.com/terraform/cli/import
