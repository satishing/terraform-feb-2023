provider "azurerm" {
  features {}
}

terraform {
  required_version = "~> 1.3.9"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.49.0"
    }
  }

  backend "azurerm" {}
}

variable "location" {}
variable "resource_group_name" {}
variable "tags" {}

resource "azurerm_resource_group" "this" {
  location = var.location
  name     = var.resource_group_name
  tags     = var.tags
}