## Create multiple environments with same terraform configuration code using workspaces.
Your backend configuration will remain same for every environment if you are using workspaces
In this case your state file name will be appended with the workspace name

### Deploy dev environment using workspaces and tfvars
```shell
$ terraform workspace new dev
$ terraform workspace list
$ terraform workspace select dev
$ terraform init --backend-config backend.config
$ terraform validate
$ terraform fmt
$ terraform plan --var-file ./envs/dev.tfvars
$ terraform apply --var-file ./envs/dev.tfvars
```

### Deploy stage environment using workspaces and tfvars
```shell
$ terraform workspace new stage
$ terraform workspace list
$ terraform workspace select stage
$ terraform init --backend-config backend.config
$ terraform validate
$ terraform fmt
$ terraform plan --var-file ./envs/stage.tfvars
$ terraform apply --var-file ./envs/stage.tfvars
```

### Deploy prod environment using workspaces and tfvars
```shell
$ terraform workspace new prod
$ terraform workspace list
$ terraform workspace select prod
$ terraform init --backend-config backend.config
$ terraform validate
$ terraform fmt
$ terraform plan --var-file ./envs/prod.tfvars
$ terraform apply --var-file ./envs/prod.tfvars
```

