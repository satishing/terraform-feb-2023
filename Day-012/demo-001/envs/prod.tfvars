resource_group_name = "rg-project-charlie-prod"
location            = "westeurope"
tags = {
  "CreatedBy"   = "Terraform"
  "Environment" = "Production"
}