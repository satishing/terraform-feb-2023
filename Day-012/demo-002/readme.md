```shell
$ terraform import azurerm_resource_group.this /subscriptions/xxxxxxxx-e5ed-43e9-8bbb-xxxxxxxxxxxx/resourceGroups/rg-project-alpha
azurerm_resource_group.this: Importing from ID "/subscriptions/xxxxxxxx-e5ed-43e9-8bbb-xxxxxxxxxxxx/resourceGroups/rg-project-alpha"...
azurerm_resource_group.this: Import prepared!
Prepared azurerm_resource_group for import
azurerm_resource_group.this: Refreshing state... [id=/subscriptions/xxxxxxxx-e5ed-43e9-8bbb-xxxxxxxxxxxx/resourceGroups/rg-project-alpha]

Import successful!

The resources that were imported are shown above. These resources are now in
your Terraform state and will henceforth be managed by Terraform.


$ terraform import azurerm_virtual_network.this /subscriptions/xxxxxxxx-e5ed-43e9-8bbb-xxxxxxxxxxxx/resourceGroups/rg-project-alpha/providers/Microsoft.Network/virtualNetworks/vnet-project-alpha
azurerm_virtual_network.this: Importing from ID "/subscriptions/xxxxxxxx-e5ed-43e9-8bbb-xxxxxxxxxxxx/resourceGroups/rg-project-alpha/providers/Microsoft.Network/virtualNetworks/vnet-project-alpha"...
azurerm_virtual_network.this: Import prepared!
Prepared azurerm_virtual_network for import
azurerm_virtual_network.this: Refreshing state... [id=/subscriptions/xxxxxxxx-e5ed-43e9-8bbb-xxxxxxxxxxxx/resourceGroups/rg-project-alpha/providers/Microsoft.Network/virtualNetworks/vnet-project-alpha]

Import successful!

The resources that were imported are shown above. These resources are now in
your Terraform state and will henceforth be managed by Terraform.


$ terraform state list
azurerm_resource_group.this
azurerm_virtual_network.this

$ terraform plan
azurerm_resource_group.this: Refreshing state... [id=/subscriptions/xxxxxxxx-e5ed-43e9-8bbb-xxxxxxxxxxxx/resourceGroups/rg-project-alpha]
azurerm_virtual_network.this: Refreshing state... [id=/subscriptions/xxxxxxxx-e5ed-43e9-8bbb-xxxxxxxxxxxx/resourceGroups/rg-project-alpha/providers/Microsoft.Network/virtualNetworks/vnet-project-alpha]

No changes. Your infrastructure matches the configuration.

Terraform has compared your real infrastructure against your configuration and found no differences, so no changes are needed.

$ terraform apply
azurerm_resource_group.this: Refreshing state... [id=/subscriptions/xxxxxxxx-e5ed-43e9-8bbb-xxxxxxxxxxxx/resourceGroups/rg-project-alpha]
azurerm_virtual_network.this: Refreshing state... [id=/subscriptions/xxxxxxxx-e5ed-43e9-8bbb-xxxxxxxxxxxx/resourceGroups/rg-project-alpha/providers/Microsoft.Network/virtualNetworks/vnet-project-alpha]

No changes. Your infrastructure matches the configuration.

Terraform has compared your real infrastructure against your configuration and found no differences, so no changes are needed.
```
