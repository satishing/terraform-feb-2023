provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "this" {
  location = "East US"
  name     = "rg-project-alpha"
  tags = {
    "CreatedBy" = "Terraform"
  }

}

resource "azurerm_virtual_network" "this" {
  address_space       = ["10.0.0.0/22"]
  location            = azurerm_resource_group.this.location
  name                = "vnet-project-alpha"
  resource_group_name = azurerm_resource_group.this.name

  subnet {
    name           = "snet-1"
    address_prefix = "10.0.0.0/24"
  }
  tags = {
    "CreatedBy" = "Terraform"
  }
}