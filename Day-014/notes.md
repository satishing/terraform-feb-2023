# Session - 014

----------------------------------------------------------------
# Topics
- CI/CD
- Pipelines
- Azure DevOps Pipeline

## CI/CD
CI and CD are two related practices that are commonly used in software development to help teams deliver better-quality software more efficiently

**CI** stands for **Continuous Integration**, which is the practice of automatically building and testing code changes every time they are made to a shared repository. This ensures that any code changes made by different developers can be integrated together in a timely and reliable manner, and that any issues or bugs can be detected early on in the development process

For example, imagine that you are working on a project with several other developers. Each time one of you makes a change to the code, that change is automatically tested to ensure that it does not break the existing code or introduce new issues. This helps to catch and fix issues quickly, before they can cause problems for the rest of the team or the end-users of the software.

**CD** stands for **Continuous Delivery or Continuous Deployment**, which is the practice of automatically releasing code changes to production environments once they have been tested and approved. Continuous Delivery means that every code change that passes automated tests is automatically deployed to a staging environment for further testing, and then can be released to production at any time. Continuous Deployment takes this a step further and automatically deploys code changes to production as soon as they pass all tests.

For example, imagine that you have a website that you are continuously developing and improving. With CD, each time a code change is made, it is automatically built, tested, and then deployed to a staging environment for further testing. Once it has been tested and approved, it can then be released to production with a single click, without any manual intervention. This helps to ensure that new features and fixes are delivered quickly and reliably, while minimizing the risk of errors or issues in production.

In summary, CI and CD are practices that help developers to work more efficiently and collaboratively, while delivering high-quality software more quickly and reliably to end-users.

## Pipelines on  different systems
Naming conventions for pipeline files can vary depending on the specific tool and the type of pipeline file being used. Here are some common file naming conventions used for pipelines on different platforms:

- Jenkins: `Jenkinsfile` : This is the default name for a pipeline file in Jenkins. It can be located in the root directory of the project or in a subdirectory.
- GitLab: `.gitlab-ci.yml` : This is the default name for a pipeline file in GitLab. It is typically located in the root directory of the project.
- GitHub: `.github/workflows/*.yml` : This is the recommended naming convention for workflow files in GitHub. Workflow files are used to define CI/CD pipelines. The files should be located in the .github/workflows directory.
- Azure DevOps: `azure-pipelines.yml` : This is the default name for a pipeline file in Azure DevOps. It should be located in the root directory of the project.

It's worth noting that these are just conventions and different names can be used depending on the specific requirements of the project.
[pipeline](./pipelines) contains pipeline example for different systems, not that those are not tested.

## Steps to create Azure DevOps Pipeline
Below are the high-level steps to create an Azure DevOps pipeline starting from creating an organization:

1. Create an Azure DevOps organization:

Go to the Azure DevOps website (https://dev.azure.com/) and sign in or create an account if you don't have one.
Click on "Create organization" and follow the prompts to create a new organization. You will need to choose a unique organization name and select a region for your organization.

2. Create a project:

Once you have created an organization, you can create a project within it.
Click on "New project" and follow the prompts to create a new project. You will need to choose a project name and select a template for your project.

3. Connect your source code repository:

To use Azure Pipelines, you will need to connect your source code repository to your project. Azure Pipelines supports several source code repositories, including Azure Repos, GitHub, and Bitbucket.
In your project, go to the "Repos" tab and click on "Import repository" to import your source code repository, or click on "Connect" to connect to an existing repository.

4. Create a pipeline:

Once your repository is connected, you can create a pipeline to build and deploy your code.
Go to the "Pipelines" tab and click on "New pipeline" to create a new pipeline. You will need to select your source code repository and choose a template for your pipeline.
Azure Pipelines provides several templates for common languages and frameworks, or you can create a custom pipeline from scratch.

5. Configure your pipeline:

Once you have created your pipeline, you can configure it to build and deploy your code as desired.
Depending on your pipeline template, you may need to specify build steps, test steps, and deployment targets.
You can also configure triggers to automatically build and deploy your code whenever changes are made to your repository. 

6. Run your pipeline:

Once your pipeline is configured, you can run it to build and deploy your code.
You can trigger your pipeline manually, or set up triggers to automatically run your pipeline whenever changes are made to your repository.
These are the high-level steps to create an Azure DevOps pipeline starting from creating an organization. The exact steps may vary depending on your specific requirements and the tools and technologies you are using.



