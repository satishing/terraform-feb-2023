provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "example" {
  location = "westindia"
  name     = "project-gamma-rg"
  tags = {
    Owner = "Satish Ingale"
  }
}