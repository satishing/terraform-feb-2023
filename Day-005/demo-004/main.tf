# providers
provider "azurerm" {
  features {}
}
provider "random" {}
provider "local" {}

#----------------------------------------------------------------
# Locals
locals {
  tags = {
    "Owner"       = "Joe Smith"
    "Environment" = "Development"
    "Project"     = "Alpha"
    "Cost-Center" = "1000-987"
    "Created By"  = "terraform"
  }
}

#----------------------------------------------------------------
# modules - resource group
module "resource_group" {
  source   = "./modules/resource_group"
  name     = "alpha-rg"
  location = "westeurope"
  tags     = local.tags
}

# modules - virtual network
module "virtual_network" {
  source               = "./modules/network"
  prefix               = "alpha"
  resource_group_name  = module.resource_group.name
  virtual_network_cidr = ["10.0.0.0/16"]
  subnet_1_cidr        = ["10.0.1.0/24"]
  subnet_2_cidr        = ["10.0.2.0/24"]
  dns_servers          = ["4.4.4.4", "8.8.8.8"]
  tags                 = local.tags

  depends_on = [module.resource_group]
}

# resource - random password for vm
resource "random_password" "this" {
  length           = 16
  min_lower        = 5
  min_upper        = 4
  numeric          = true
  special          = true
  override_special = "!#$@"
}

# modules - virtual machine
module "virtual_machine" {
  source              = "./modules/compute"
  prefix              = "alpha"
  resource_group_name = module.resource_group.name
  vm_size             = "Standard_F2"
  admin_username      = "cloudadmin"
  admin_password      = random_password.this.result
  subnet_id           = module.virtual_network.subnet_1_id
  source_image_reference = {
    image_publisher = "MicrosoftWindowsServer"
    image_offer     = "WindowsServer"
    image_sku       = "2016-Datacenter"
    image_version   = "latest"
  }
  tags = local.tags

  depends_on = [module.resource_group, module.virtual_network, random_password.this]
}

# resource - network security group for vm
resource "azurerm_network_security_group" "this" {
  name                = "alpha-vn-nsg"
  location            = module.resource_group.location
  resource_group_name = module.resource_group.name

  security_rule {
    name                       = "alpha-vm-rule-allow-rdp-1"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags       = local.tags
  depends_on = [module.resource_group]
}

# resource - association of network security group for vm and subnet in which vm is created
resource "azurerm_subnet_network_security_group_association" "this" {
  subnet_id                 = module.virtual_network.subnet_1_id
  network_security_group_id = azurerm_network_security_group.this.id

  depends_on = [module.virtual_network, module.virtual_machine]
}

resource "azurerm_network_interface_security_group_association" "example" {
  network_interface_id      = module.virtual_machine.nic_id
  network_security_group_id = azurerm_network_security_group.this.id

  depends_on = [module.virtual_network, module.virtual_machine]
}

# resource - random password will be stored in this file
resource "local_sensitive_file" "this" {
  content    = random_password.this.result
  filename   = "${path.root}/password.txt"
  depends_on = [random_password.this]
}

#----------------------------------------------------------------
# output
output "vm_public_ip" {
  description = "Public ip address of the vm"
  value       = module.virtual_machine.vm_public_ip

  depends_on = [module.virtual_machine]
}