variable "prefix" {
  description = "Prefix with which the nnetwork resources should be created"
  type        = string
}

variable "resource_group_name" {
  type        = string
  description = "Name of the resource group where the virtual machine will be created"
}

variable "vm_size" {
  type        = string
  description = "Size of the virtual machine"
}

variable "admin_username" {
  description = "Username of the local administrator account on the virtual machine"
  type        = string
}

variable "admin_password" {
  description = "Password of the local administrator account on the virtual machine"
  type        = string
}

variable "subnet_id" {
  description = "ID of the subnet where the virtual machine will be deployed"
  type        = string
}

variable "source_image_reference" {
  # Heredoc example below
  description = <<-EOT
    The source image reference with following fields
    - publisher of the Windows image to use for the virtual machine
    - offer of the Windows image to use for the virtual machine
    - SKU of the Windows image to use for the virtual machine
    - version of the Windows image to use for the virtual machine
  EOT

  type = object({
    image_publisher = string
    image_offer     = string
    image_sku       = string
    image_version   = string
  })
}

variable "storage_account_type" {
  description = "The storage account type of disk for the virtual machine"
  type        = string
  default     = "Standard_LRS"
}

variable "tags" {
  description = "A mapping of tags which should be assigned to the network resource."
  type        = map(any)
}