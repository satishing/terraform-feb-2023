output "vm_name" {
  description = "Name of the virtual machine"
  value       = azurerm_windows_virtual_machine.this.name
}

output "vm_public_ip" {
  description = "Public ip of the virtual machine"
  value       = azurerm_windows_virtual_machine.this.public_ip_address
}

output "vm_private_ip" {
  description = "Private ip of the virtual machine"
  value       = azurerm_network_interface.this.private_ip_address
}

output "nic_id" {
  description = "Network interface card id"
  value       = azurerm_network_interface.this.id
}
