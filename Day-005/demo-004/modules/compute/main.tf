data "azurerm_resource_group" "this" {
  name = var.resource_group_name
}

resource "azurerm_public_ip" "this" {
  name                = "${var.prefix}-vm-pip"
  resource_group_name = data.azurerm_resource_group.this.name
  location            = data.azurerm_resource_group.this.location
  allocation_method   = "Dynamic"

  tags = var.tags
}

resource "azurerm_network_interface" "this" {
  name                = "${var.prefix}-vm-nic"
  resource_group_name = data.azurerm_resource_group.this.name
  location            = data.azurerm_resource_group.this.location

  ip_configuration {
    name                          = "${var.prefix}-vm-ipconfig"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.this.id
  }

  tags = var.tags
}

resource "azurerm_windows_virtual_machine" "this" {
  name                  = "${var.prefix}-vm"
  resource_group_name   = data.azurerm_resource_group.this.name
  location              = data.azurerm_resource_group.this.location
  network_interface_ids = [azurerm_network_interface.this.id]
  size                  = var.vm_size
  admin_username        = var.admin_username
  admin_password        = var.admin_password

  os_disk {
    name                 = "${var.prefix}-vm-osdisk"
    caching              = "ReadWrite"
    storage_account_type = var.storage_account_type
  }

  source_image_reference {
    publisher = var.source_image_reference.image_publisher
    offer     = var.source_image_reference.image_offer
    sku       = var.source_image_reference.image_sku
    version   = var.source_image_reference.image_version
  }

  tags = var.tags
}