data "azurerm_resource_group" "this" {
  name = var.resource_group_name
}

resource "azurerm_virtual_network" "this" {
  name                = "${var.prefix}-vnet"
  resource_group_name = data.azurerm_resource_group.this.name
  location            = data.azurerm_resource_group.this.location
  address_space       = var.virtual_network_cidr
  dns_servers         = var.dns_servers

  tags = var.tags
}

resource "azurerm_subnet" "subnet_1" {
  name                 = "${var.prefix}-subnet-1"
  resource_group_name  = data.azurerm_resource_group.this.name
  virtual_network_name = azurerm_virtual_network.this.name
  address_prefixes     = var.subnet_1_cidr
}

resource "azurerm_subnet" "subnet_2" {
  name                 = "${var.prefix}-subnet-2"
  resource_group_name  = data.azurerm_resource_group.this.name
  virtual_network_name = azurerm_virtual_network.this.name
  address_prefixes     = var.subnet_2_cidr
}