variable "prefix" {
  description = "Prefix with which the nnetwork resources should be created"
  type        = string
}

variable "resource_group_name" {
  description = "The name of the resource group in which to create the virtual network"
  type        = string
}

variable "virtual_network_cidr" {
  description = "The CIDR block for the virtual network"
  type        = list(string)
}

variable "dns_servers" {
  description = "List of IP addresses of DNS servers"
  type        = list(string)
}

variable "subnet_1_cidr" {
  description = "The CIDR block for the first subnet"
  type        = list(string)
}

variable "subnet_2_cidr" {
  description = "The CIDR block for the second subnet"
  type        = list(string)
}

variable "tags" {
  description = "A mapping of tags which should be assigned to the network resource."
  type        = map(any)
}