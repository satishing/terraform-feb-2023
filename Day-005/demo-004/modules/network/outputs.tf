output "address_space" {
  description = "The virtual network address space"
  value       = azurerm_virtual_network.this.address_space
}

output "id" {
  description = "The virtual network configuration id"
  value       = azurerm_virtual_network.this.address_space
}

output "name" {
  description = "The virtual network name"
  value       = azurerm_virtual_network.this.address_space
}


output "subnet_1_id" {
  description = "The subnet id of subnet1"
  value       = azurerm_subnet.subnet_1.id
}

output "subnet_2_id" {
  description = "The subnet id of subnet2"
  value       = azurerm_subnet.subnet_2.id
}