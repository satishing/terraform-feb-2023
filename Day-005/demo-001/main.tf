# Get value from the list of elements
variable "my_list" {
  type    = list(any)
  default = ["foo", "bar", "baz", "far"]
}

output "first_element_with_index" {
  value = var.my_list[0]
}

output "first_element_of_list" {
  value     = element(var.my_list, 0)
  sensitive = true
}
#----------------------------------------------------------------
# Get value from map
variable "my_map" {
  type = map(any)
  default = {
    key1 = "value1"
    key2 = "value2"
    key3 = "value3"
  }
}

output "value_for_key2" {
  value = var.my_map["key2"]
}

#----------------------------------------------------------------
# Refer values from complex  map/object

variable "my_object" {
  type = object({
    name = string
    age  = number
    address = object({
      street  = string
      city    = string
      country = string
    })
  })

  default = {
    name = "John Doe"
    age  = 30
    address = {
      street  = "123 Main St"
      city    = "Anytown"
      country = "USA"
    }
  }
}

output "age" {
  #  value = var.my_object.age
  value = var.my_object["age"]
}

output "city" {
  #  value = var.my_object.address.city
  #  value = var.my_object.address["city"]
  value = var.my_object["address"]["city"]
}

#----------------------------------------------------------------
variable "my_list_of_maps" {
  type = list(object({
    name  = string
    email = string
  }))

  default = [
    {
      name  = "John Doe"
      email = "johndoe@example.com"
    },
    {
      name  = "Ajay Pawar"
      email = "ajay@example.com"
    },
    {
      name  = "Bob Smith"
      email = "bobsmith@example.com"
    }
  ]
}

output "first_element" {
  value = "var.my_list_of_maps[1]"
}

output "email_of_first_element" {
  value = "var.my_list_of_maps[1].email"
}

output "email_of_all_elements" {
  #  value = var.my_list_of_maps.*.email
  value = var.my_list_of_maps[*].email
}










