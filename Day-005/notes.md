# Session - 005

## Recap
### Refactoring a code
- `providers.tf` azurerm, random, local, kubernetes, etc
- `variables.tf`
- `outputs.tf`
- `locals.tf`
- `main.tf`, `vpc.tf` etc

### Types of variables
- string
- number
- bool
- list
  - set
  - tuple
- map
  - object

### sensitive

Often you need to configure your infrastructure using sensitive or secret information such as usernames, passwords, API tokens, or Personally Identifiable Information (PII). When you do so, you need to ensure that you do not accidentally expose this data in CLI output, log output, or source control.

```terraform
variable "db_password" {
  description = "Password to connect to database"
  type = string
  
  sensitive = true
}
```
or

```terraform
output "kube_config" {
  value     = azurerm_kubernetes_cluster.example.kube_config_raw
  sensitive = true
}
```

### Providers
- local
- random

----------------------------------------------------------------
## Interpolation

https://www.terraform.io/language/expressions/strings#interpolation

A ${ ... } sequence is an interpolation, which evaluates the expression given between the markers, converts the result to a string if necessary, and then inserts it into the final string:

```terraform
locals {
  name = "Satish"
  surname = "Ingale"
}

output "fullname" {
  value = "Full name is ${locals.name} ${locals.surname}"
}
```

----------------------------------------------------------------
## Referencing lists and maps
https://www.terraform.io/language/expressions/references 

----------------------------------------------------------------
## Terraform modules

Modules are containers for multiple resources that are used together. A module consists of a collection of `.tf` and/or `.tf.json` files kept together in a directory.

Modules are the main way to package and reuse resource configurations with Terraform.

https://developer.hashicorp.com/terraform/language/modules


### Module Development

https://developer.hashicorp.com/terraform/language/modules/develop

----------------------------------------------------------------

## Demos
- [demo-001](./demo-001) - Reference values from list, maps, complex objects
- [demo-002](./demo-002) - Data block in terraform
- [demo-003](./demo-003) - Interpolation
- [demo-004](./demo-004) - Module

## Reference docs
- https://www.terraform.io/language/expressions/references