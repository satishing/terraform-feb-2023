provider "azurerm" {
  features {}
}

data "azurerm_storage_account" "example" {
  name                = "somerandomstorageacc001"
  resource_group_name = "BALA-RG"
}

output "id" {
  value = data.azurerm_storage_account.example.id
}