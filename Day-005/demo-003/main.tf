locals {
  name     = "Satish"
  lastname = "Ingale"
}

output "full_name" {
  #  value = "${local.name}-${local.lastname}"
  value = "${local.name} ${local.lastname}"
}

