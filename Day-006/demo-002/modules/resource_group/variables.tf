variable "location" {
  description = "The azure region where the resource group should exist"
  type        = string
}

variable "name" {
  description = "The name which should be used for this resource group"
  type        = string
}

variable "tags" {
  description = "A mapping of tags which should be assigned to the resource group."
  type        = map(any)
}