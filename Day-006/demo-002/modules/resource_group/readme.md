## Resource Group Module

This module creates a resource group in azure

### Requirements
This module is tested with below versions

| name      | Version |
|-----------|---------|
| terraform | ~>1.3.9 |
| azurerm   | v3.46.0 |


### Output Variables

| Variable Name | Description                    | 
|---------------|--------------------------------|
| name          | Resource group name            | 
| location      | Location of the resource group | 
| id            | ID for the resource group      |

### How to use the module?
```terraform
module "resource_group" {
  source = "./modules/resource_group"
  name = "rg-alpha"
  location = "westeurope"
  tags = { "Owner" = "John"}
}
```