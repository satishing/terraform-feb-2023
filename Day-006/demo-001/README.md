# Heading
## Heading
### Heading
#### Heading

## List Example
- Mango
- Grapes
- Oranges

* School
    - Lexicon
    - ABC 
* Hospital
* Home

This is a normal text

**This is bold text**

_This is italic_

```
resource "azurerm_resource_group" "this" {
    name = "rg-aalpha"
    location = "eastus"
}
```

Always write `provider` at the top of terraform configuration.

Check my LinkedIn profile [here](https://linkedin.com/satish-ingale)

| Firstname | Lastname |
|-----------|----------|
| Satish    | Ingale   |

![architecture diagram](arch.jpeg)