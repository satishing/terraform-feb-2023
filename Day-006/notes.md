
### Module Development

https://developer.hashicorp.com/terraform/language/modules/develop

----------------------------------------------------------------

## Demos
- [demo-001](./demo-001) - Makedown syntax to write README
- [demo-002](./demo-002) - Readme file for module
- [demo-003](./demo-003) - Using module from terraform registry

## Reference docs
- https://www.markdownguide.org/basic-syntax/
- https://registry.terraform.io/
- https://github.com/terraform-docs/terraform-docs