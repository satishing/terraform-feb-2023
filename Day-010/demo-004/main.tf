locals {
  names                = ["ajay", "bala", "ramesh", "vitthal"]
  generated_map        = { for name in local.names : name => length(name) }
  capitalized_names    = { for name in local.names : name => upper(name) }
  indexed_names        = [for idx, name in local.names : "${name} is at ${idx} position"]
  indexed_map_of_names = { for idx, name in local.names : idx => name }
}

output "generated_map" {
  value = local.generated_map
}
output "capitalized_names" {
  value = local.capitalized_names
}
output "indexed_names" {
  value = local.indexed_names
}
output "indexed_map_of_names" {
  value = local.indexed_map_of_names
}