provider "azurerm" {
  features {}
}

variable "env" {
  description = "Specifies the environment, `Dev` `Prod"
}

resource "azurerm_virtual_machine" "example" {
  vm_size = var.env == "Prod" ? "Standard_D2_v3" : "Standard_B11s"
}

#--------------------------

variable "environment" {
  description = "Specifies the environment, `Dev`, Test` or `Prod"
}

resource "azurerm_virtual_machine" "this" {
  #  vm_size = var.environment == "Prod" ? "Standard_D2_v3" : var.environment == "Test" ? "Standard_D2_v1" : "Standard_B11s"
  vm_size = (var.environment == "Prod" || var.environment == "Test") ? "Standard_D2_v3" : "Standard_B11s"
}

#----------------------------------------------------------------
variable "create_storage_account" {
  description = "Specifies whether to create the storage account, possible values are `true` or `false"
  type        = bool
}

resource "azurerm_storage_account" "example" {
  count                    = var.create_storage_account ? 1 : 0
  name                     = "mystorageaccountname"
  resource_group_name      = "my-resources"
  location                 = "west-europe"
  account_tier             = "Standard"
  account_replication_type = "GRS"
}

#----------------------------------------------------------------





