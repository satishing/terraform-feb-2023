provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "example" {
  name     = "example-resources"
  location = "westeurope"
  tags = {
    environment = "Production"
  }
}

resource "azurerm_virtual_network" "this" {
  name                = "example-network"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  address_space       = ["10.0.0.0/16"]
  dns_servers         = ["10.0.0.4", "10.0.0.5"]

  dynamic "subnet" {
    for_each = var.subnet_details
    iterator = snet
    content {
      name           = snet.value.name
      address_prefix = snet.value.address_prefix
    }
  }

  tags = {
    environment = "Production"
  }
}


