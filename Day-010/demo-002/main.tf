variable "names" {
  type    = list(string)
  default = ["ajay", "bala", "ramesh", "vitthal"]
}

locals {
  squares = [for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] : pow(i, 2)]
  names   = [for name in var.names : title(name)]
}

output "squares" {
  value = local.squares
}

output "names" {
  value = local.names
}
#----------------------------------------------------------------
data "azurerm_virtual_machine" "example" {
  name                = "example-vm"
  resource_group_name = "example-rg"
}

output "nic_ip_addresses" {
  value = [for nic in data.azurerm_virtual_machine.example : nic.private_ip_address]
}
#----------------------------------------------------------------

