variable "enable_monitoring" {
  description = "Enable monitoring for the cluster."
  validation {
    condition     = can(regex("^([t][r][u][e]|[f][a][l][s][e])$", var.enable_monitoring))
    error_message = "The boolean_variable must be either true or false."
  }
}

variable "environment" {
  description = "Specifies environment"
  type        = string
  validation {
    condition     = contains(["Prod", "Dev", "Stage"], var.environment)
    error_message = "Environment must Prod, Dev or Stage"
  }
}

locals {
  env_type = var.environment == "Prod" ? "PRODUCTION" : "NON-PROD"
}

output "enable_monitoring" {
  value = var.enable_monitoring
}
output "env_type" {
  value = local.env_type
}