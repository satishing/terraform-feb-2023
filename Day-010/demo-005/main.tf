variable "names" {
  default = [
    {
      firstname = "Satish"
      lastname  = "Ingale"
    },
    {
      firstname = "Ajay"
      lastname  = "Pawar"
    },
    {
      firstname = "Soumya"
      lastname  = "Sahu"
    }
  ]
}

output "for_loop_expression" {
  value = [for i in var.names : i.lastname]
}

output "splat_expression" {
  value = var.names[*].lastname
}

#----------------------------------------------------------------

# [for o in var.list : o.interfaces[0].name] is same as  var.list[*].interfaces[0].name