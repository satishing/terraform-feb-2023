locals {
  provided_locations = ["us-east", "eu-north", "us-west", "swz-north", "us-north-central", "kor-central"]
  allowed_locations  = [for i in local.provided_locations : i if startswith(i, "us")]
}

variable "location" {}

resource "azurerm_resource_group" "this" {
  count    = contains(local.allowed_locations, var.location) ? 1 : 0
  name     = "my-resource-group"
  location = var.location
}

output "allowed_locations" {
  value = local.allowed_locations
}

